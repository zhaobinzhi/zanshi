import React, { ReactElement, useState, useEffect, useRef, FC } from 'react';
import Style from './index.module.scss';
import Table from '@/components/Table';
import moment from 'moment';
import { DatePicker, Form, Input } from 'antd';
import { RecordListType, EditableCellProps } from '@/typings/dataPlayback';
import { deleteMedication, modifyMedication, addMedication, queryMedicationRecordAndAlarm } from '@/api/medicationRecord';
import annotationQuick from '@/assets/icon/ico-quick-annotation-16.svg';
import PlusIcon from '@/assets/icon/ico-add2.svg';
import dataEdit1 from '@/assets/bundles/ico-edit-16.svg';
import dataEdit2 from '@/assets/icon/dataEdit.svg';
import deletBtn1 from '@/assets/icon/ico-delete2-16.svg';
import arrowUp from '@/assets/icon/ico-arrow-up-16.svg';

import deletBtn2 from '@/assets/icon/delet.svg';
import ModelComponent from '@/components/Modal';
import MessageComponent from '@/components/Message';
import EmptyComponent from '@/components/EmptyComponent/index';
import { ClockCircleOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { stateTyps } from '@/reducers/statesType';
import { convenientMark } from '@/api/operationInfoApi';

import Tooltip from './Tooltip';
// import dayjs from 'dayjs';
const EditableCell: React.FC<EditableCellProps> = ({ editing, dataIndex, children, ...restProps }) => {
	const inputNode =
		dataIndex === 'time' ? (
			<DatePicker
				style={{ width: 'calc(100% + 50px)' }}
				suffixIcon={<ClockCircleOutlined />}
				allowClear={false}
				mode="date"
				placeholder="年/月/日 00:00:00"
				dropdownClassName="PublicDatePickerDropdown"
				format="YYYY/MM/DD HH:mm:ss"
				showTime
				showNow={false}
			/>
		) : dataIndex === 'eventName' ? (
			<Input
				onClick={event => {
					// 阻止事件的传递
					event.stopPropagation();
				}}
				style={{ width: 'calc(100% - 4px)' }}
				placeholder="请输入事件"
			/>
		) : (
			''
		);
	return (
		<td {...restProps} className="editableCell">
			{editing ? (
				<div>
					<Form.Item
						name={dataIndex}
						// style={{ marginTop: 12 }}
						rules={[
							{
								required: dataIndex === 'time' || dataIndex === 'eventName',
								message: dataIndex === 'time' ? '请输入时间!' : dataIndex === 'eventName' ? '请输入事件名称!' : '',
							},
						]}
					>
						{inputNode}
					</Form.Item>
				</div>
			) : (
				children
			)}
		</td>
	);
};
const DataAnnotations: FC<{ alarmData?: { [key: string]: string } }> = (props): ReactElement => {
	const [form] = Form.useForm();
	const [delFile, setDelFile] = useState<RecordListType>(); // 点击删除弹窗保存的对象
	const [sortName, setSortName] = useState<string>('drugName');
	const [dataAnnotations, setDataAnnotations] = useState<Array<RecordListType>>([]); // table数据
	const [hoverIdObj, sethoverIdObj] = useState({
		// 划过编辑按钮 删除按钮
		hoverEditedId: '',
		hoverDeledId: '',
	});
	// 用于判断操作的那一行
	const [editingKey, setEditingKey] = useState('');
	const operateRoomId = useSelector((state: stateTyps) => {
		return state.operationInfoReducer.currentOperationRoomInfo?.chief.operateRoomId || '';
	});
	const isCanAdd = useRef<boolean>(true);
	const judgeResize = useSelector((store: stateTyps) => store.publicUtilsReducer.judgeResize);
	const tableRef = useRef<HTMLDivElement>(null);
	const dataAnnotationsRef = useRef<HTMLDivElement>(null);
	const deputyOccupy = useSelector((store: stateTyps) => {
		return store.operationInfoReducer?.currentOperationRoomInfo?.deputyOccupy;
	});

	const dataAnnotationTableTopHeight = useRef<string | number>(175);

	const canAxios = useRef<boolean>(true);
	const alarmsleepTime = useRef<NodeJS.Timeout>();

	/** 表头渲染 */
	const columnsTitleRender = (title: string): ReactElement => {
		switch (title) {
			case '类型':
				return (
					<div
						id="drugColumnStyle"
						style={{ cursor: 'pointer', color: '#51BDDF' }}
						onClick={() => {
							eventClick('drugName', sortName);
						}}
					>
						类型 &nbsp;
						{sortNameComponent(sortName)}
					</div>
				);
			case '时间':
				return (
					<div
						id="timeColumnStyle"
						style={{ cursor: 'pointer', color: '#51BDDF' }}
						onClick={() => {
							eventClick('times', sortName);
						}}
					>
						时间 &nbsp;
						{sortTimeComponent(sortName)}
					</div>
				);
			default:
				return <div />;
		}
	};
	/** 排序组件 */
	const sortNameComponent = (value: string) => {
		switch (value) {
			case 'time':
				return <img src={arrowUp} alt="" />;
			case 'drugName':
				return <img style={{ transform: 'rotate(180deg)' }} src={arrowUp} alt="" />;

			default:
				return null;
		}
	};

	const sortTimeComponent = (value: string) => {
		switch (value) {
			case 'timeAsc':
				return <img src={arrowUp} alt="" />;
			case 'timeDesc':
				return <img style={{ transform: 'rotate(180deg)' }} src={arrowUp} alt="" />;
			default:
				return null;
		}
	};
	// 点击头部排序  类型  上time  下drugName    时间 上timeAsc  下timeDesc
	function eventClick(type: 'drugName' | 'times', sortName: any) {
		if (type === 'drugName') {
			// 点击事件
			if (sortName === 'timeAsc' || sortName === 'timeDesc') {
				// 从时间点到事件
				setSortName('drugName');
			} else {
				setSortName(sortName === 'time' ? 'drugName' : 'time');
			}
		}
		if (type === 'times') {
			// 点击时间
			if (sortName === 'time' || sortName === 'drugName') {
				// 从事件点到时间
				setSortName('timeDesc');
			} else {
				setSortName(sortName === 'timeAsc' ? 'timeDesc' : 'timeAsc');
			}
		}
	}

	function handleMouseEnter(index: string, type: 'edit' | 'del') {
		if (type === 'edit') {
			sethoverIdObj({ ...hoverIdObj, ...{ hoverEditedId: index } });
		} else {
			sethoverIdObj({ ...hoverIdObj, ...{ hoverDeledId: index } });
		}
	}
	function handleMouseLeave() {
		sethoverIdObj({
			hoverEditedId: '',
			hoverDeledId: '',
		});
	}
	function markTag() {
		convenientMark({ operateRoomId }).then(res => {
			if (Number(res.status) === 200) {
				MessageComponent({ content: '新增成功!', type: 'success' });
				queryDrugRecordList();
			}
		});
	}

	const columns = [
		{
			title: '事件名称',
			dataIndex: 'eventName',
			width: '45%',
			editable: true,
			render: (text: string) => {
				return (
					<Tooltip title={text}>
						<span className="drugNameBox">{text}</span>
					</Tooltip>
				);
			},
		},
		{
			title: columnsTitleRender('时间'),
			dataIndex: 'time',
			width: '29%',
			editable: true,
			render: (text: string) => {
				if (!text) {
					return <span />;
				} else {
					return <span className="createTime">{moment(text).format('YYYY/MM/DD HH:mm:ss')}</span>;
				}
			},
		},
		{
			title: columnsTitleRender('类型'),
			dataIndex: 'type',
			width: 70,
			editable: true,
			align: 'center',
			render: (text: string, record: RecordListType) => {
				return (
					<span>
						{record.eventType === 0 ? (
							<span className="drugNameType">事件</span>
						) : (
							<span className="drugNameType" style={{ background: '#A80925', color: '#FAFBFD' }}>
								报警
							</span>
						)}
					</span>
				);
			},
		},
		{
			title: '操作',
			width: 70,
			align: 'center',
			render: (_: any, record: RecordListType) => {
				const editable = record.medicationRecordId === editingKey;
				return record.eventType === 0 ? (
					editable ? (
						<span>
							<a
								className="confirmBtn"
								onClick={event => {
									event.stopPropagation();
									if (canAxios.current) {
										save(record.medicationRecordId);
									}
								}}
							>
								确定
							</a>
							<a
								className="cancleBtn"
								onClick={event => {
									event.stopPropagation();
									cancel(record);
								}}
							>
								取消
							</a>
						</span>
					) : (
						!deputyOccupy && (
							<div>
								<img
									style={{ cursor: 'pointer' }}
									src={hoverIdObj.hoverEditedId === record.medicationRecordId ? dataEdit2 : dataEdit1}
									onMouseEnter={() => handleMouseEnter(record.medicationRecordId, 'edit')}
									onMouseLeave={handleMouseLeave}
									onClick={() => edit(record)}
									alt="edit"
								/>
								<img
									src={hoverIdObj.hoverDeledId === record.medicationRecordId ? deletBtn2 : deletBtn1}
									className="deletIcons"
									alt="delet"
									onMouseEnter={() => handleMouseEnter(record.medicationRecordId, 'del')}
									onMouseLeave={handleMouseLeave}
									onClick={e => clickSetDelFile(e, record)}
								/>
							</div>
						)
					)
				) : (
					// 临时写
					<div>
						<img style={{ opacity: 0 }} src={dataEdit2} alt="edit" />
					</div>
				);
			},
		},
	];

	/** 添加 */
	const addDataClick = () => {
		if (!isCanAdd.current) return;
		if (dataAnnotations.find((x: RecordListType) => x.addStatus)) {
			MessageComponent({ content: '只能新增一条数据!', type: 'warning' });
			return;
		}
		const currentTime = moment().format('YYYY/MM/DD HH:mm:ss');
		// 组合指定日期和当前时间的时分秒
		// const combinedDateTime = moment(moment(eventListRD?.startTimeString, 'YYYY/MM/DD HH:mm:ss').format('YYYY/MM/DD') + ' ' + currentTime).format('YYYY/MM/DD HH:mm:ss');
		const newData: RecordListType = {
			eventName: '',
			eventType: 0,
			medicationRecordId: '',
			time: currentTime,
			index: 0,
			addStatus: true, // 说明是新增数据
		};
		setDataAnnotations(dataAnnotations === undefined ? [newData] : [newData, ...dataAnnotations]);
		edit(newData);
	};

	/** 保存 */
	const save = async (key: string) => {
		canAxios.current = false;
		const row = (await form.validateFields()) as RecordListType;
		const addModify = {
			...row,
		};
		if (key) {
			// 修改逻辑
			const medicationTime = moment(addModify.time).format('YYYY-MM-DD HH:mm:ss');
			modifyMedication({
				drugName: addModify.eventName,
				medicationRecordId: key,
				medicationTime: medicationTime,
			})
				.then((data: any) => {
					const status: number = data?.status;
					if (Number(status) === 200) {
						setEditingKey('');
						queryDrugRecordList();
						form.resetFields();
						MessageComponent({ content: '修改成功!', type: 'success' });
					} else {
						data?.msg &&
							MessageComponent({
								type: 'error',
								content: data?.msg,
							});
					}
				})
				.finally(() => {
					canAxios.current = true;
				});
		} else {
			// 新增逻辑
			const medicationTime = moment(addModify.time).format('YYYY-MM-DD HH:mm:ss');
			addMedication({
				drugName: addModify.eventName,
				operateRoomId: operateRoomId,
				medicationTime: medicationTime,
			})
				.then((data: any) => {
					const status: number = data?.status;
					if (Number(status) === 200) {
						queryDrugRecordList();
						setEditingKey('');
						form.resetFields();
						MessageComponent({ content: '新增成功!', type: 'success' });
					} else {
						data?.msg &&
							MessageComponent({
								type: 'error',
								content: data?.msg,
							});
					}
				})
				.finally(() => {
					canAxios.current = true;
				});
		}
	};

	useEffect(() => {
		if ((props.alarmData?.ALARM_FLAG as any)?.ALARM_FLAG === 'true') {
			// 为了后端
			alarmsleepTime.current = setTimeout(() => {
				queryDrugRecordList();
			}, 1000);
		}
	}, [props.alarmData]);

	useEffect(() => {
		queryDrugRecordList();
	}, [sortName]);

	useEffect(() => {
		if (operateRoomId) {
			setSortName('drugName');
			queryDrugRecordList();
		}
		return () => {
			if (alarmsleepTime.current) {
				clearTimeout(alarmsleepTime.current);
			}
		};
	}, [operateRoomId]);

	useEffect(() => {
		// 编辑自适应输入框和日期框宽度计算  下面sethoverIdObj为了刷新页面
		if (tableRef?.current?.offsetWidth && dataAnnotationsRef?.current) {
			dataAnnotationTableTopHeight.current = dataAnnotationsRef?.current?.offsetHeight - 87;
			sethoverIdObj({
				hoverEditedId: '',
				hoverDeledId: '',
			});
		}
	}, [judgeResize]);

	// 点击事件列表传值趋势->波形->体征
	// const clickAnnotationData = (record: RecordListType) => {
	// 	const annotationObj = {
	// 		annotationTime: moment(record.time).format('YYYY/MM/DD HH:mm:ss'),
	// 	};
	// 	annotationOnclick && annotationOnclick(annotationObj);
	// };

	/** 取消 */
	const cancel = (record: RecordListType | undefined) => {
		if (record?.addStatus) {
			setDataAnnotations(prevDataAnnotations => prevDataAnnotations.slice(1));
		}
		setEditingKey('');
	};

	/** 查询接口 */
	const queryDrugRecordList = () => {
		if (!operateRoomId) return;
		queryMedicationRecordAndAlarm({
			startTime: '',
			operateRoomId: operateRoomId,
			sortName: sortName,
			endTime: '',
		}).then(res => {
			const status: number = res?.status;
			if (Number(status) === 200) {
				setDataAnnotations([...res.data]);
			} else {
				MessageComponent({ content: '查询失败!', type: 'error' });
			}
		});
	};

	/** 编辑 */
	const edit = (record: Partial<RecordListType> & { medicationRecordId: React.Key }) => {
		// 解决先新增了一点，然后点击其它编辑按钮，会同时出现两个编辑状态，编辑或者删除其它行时候，如果有新增列删掉它
		if (dataAnnotations.find((x: RecordListType) => x.addStatus)) {
			setDataAnnotations(prevDataAnnotations => prevDataAnnotations.slice(1));
		}
		record.time = record.time ? moment(moment(record.time).format('YYYY/MM/DD HH:mm:ss'), 'YYYY/MM/DD HH:mm:ss') : '';
		form.setFieldsValue({ ...record });
		setEditingKey(record.medicationRecordId);
	};
	// 删除弹窗
	const clickSetDelFile = (e: React.MouseEvent<HTMLImageElement, MouseEvent>, record: RecordListType) => {
		// 解决先新增了一点，然后点击其它编辑按钮，会同时出现两个编辑状态，编辑或者删除其它行时候，如果有新增列删掉它
		if (dataAnnotations.find((x: RecordListType) => x.addStatus)) {
			setDataAnnotations(prevDataAnnotations => prevDataAnnotations.slice(1));
		}
		e.stopPropagation();
		setDelFile(record);
	};

	// 删除逻辑
	function sureEvent() {
		const { medicationRecordId } = delFile as RecordListType;
		deleteMedication(medicationRecordId.toString()).then(data => {
			const status: number = data?.status;
			if (Number(status) === 200) {
				queryDrugRecordList();
				setDelFile(undefined);
				MessageComponent({ content: '删除成功!', type: 'success' });
			} else {
				MessageComponent({ content: '删除失败!', type: 'error' });
				setDelFile(undefined);
			}
		});
	}
	// 关闭弹窗
	function cancleEvent() {
		setDelFile(undefined);
	}
	const mergedColumns: any = columns.map(col => {
		if (!col.editable) {
			return col;
		}
		return {
			...col,
			onCell: (record: { addStatus: boolean; medicationRecordId: string }) => ({
				record,
				dataIndex: col.dataIndex,
				editing: record.addStatus || record.medicationRecordId === editingKey,
			}),
		};
	});

	return (
		<div ref={dataAnnotationsRef} className={Style.dataAnnotations}>
			<div className={Style.dataPlayTitle}>
				事件列表
				{!deputyOccupy && (
					<div className={Style.addDataBoxZong}>
						<div className={isCanAdd.current ? Style.addDataBox : Style.disAddDataBox} onClick={markTag}>
							<img className={Style.PlusIcon} src={annotationQuick} alt="" />
							<span className={Style.addDataBtn}>一键标注</span>
						</div>
						<div className={isCanAdd.current ? Style.addDataBox : Style.disAddDataBox} onClick={addDataClick}>
							<img className={Style.PlusIcon} src={PlusIcon} alt="" />
							<span className={Style.addDataBtn}>添加事件</span>
						</div>
					</div>
				)}
			</div>
			<div ref={tableRef} className={Style.tableContent} style={{ padding: dataAnnotations && dataAnnotations.length > 2 ? '0 0 0 10px' : '0 10px 0 10px' }}>
				{dataAnnotations?.length && dataAnnotations ? (
					<Form form={form} component={false}>
						<Table
							className="eventListTable"
							rowKey={record => record.index}
							scroll={dataAnnotations?.length > 3 ? { y: dataAnnotationTableTopHeight.current } : {}}
							columns={mergedColumns}
							dataSource={(dataAnnotations?.length && dataAnnotations) as any}
							components={{
								body: {
									cell: EditableCell,
								},
							}}
						/>
					</Form>
				) : (
					<div className={Style.ModelBox}>
						<EmptyComponent emptyText="当前暂无标注/报警信息" />
					</div>
				)}
				<ModelComponent width={600} open={delFile ? true : false} titleFormat="Alert" title="删除" onSure={sureEvent} onCancel={cancleEvent}>
					{delFile ? `请确定是否删除 "${delFile.eventName}" 该事件？` : null}
				</ModelComponent>
			</div>
		</div>
	);
};
export default DataAnnotations;
